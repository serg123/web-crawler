export class MirrorLoader{
    constructor(apiUrl){
        this.apiUrl=apiUrl;
    }
    get(url){
        return new Promise((resolve,reject)=>{
            url=encodeURIComponent(url);
            fetch(this.apiUrl+'?url='+url).then(response=>{
                if(response.ok)
                    return response.text();
                else
                    throw "response is not OK!";
            }).then(text=>{
                resolve(text);
            }).catch(error=>{
                reject(error);
            });
        });
    }
    
}

export default class ResponseParser extends MirrorLoader{
    constructor(apiUrl='api/mirror.php'){
        super(apiUrl);
        this.calls=[];      //records calls [methodName, ...args]
        this.callIndex=0;   //used in run to track current call in run()
        this.data=[];       //stores {colName,selectedNodes,displayFunc}
        this.headers=new Set(); //stores column names
        this.result=new Map();  //stores resulting table data after process()
    }
    load(url,mime){         //record call
        this.calls.push(['_load',url,mime]);
    }
    query(colName,selector,displayFunc){    //record call
        this.calls.push(['_query',colName,selector,displayFunc]);
    }
    run(readyCallback){     //plays calls + process in the end
        if(this.callIndex<this.calls.length){
            let call=this.calls[this.callIndex++];
            if(call[0]=='_load')
                this._load(...call.slice(1)).then(()=>this.run(readyCallback));
            else{
                this._query(...call.slice(1));
                this.run(readyCallback);
            }
        }
        else{
            this.process();
            readyCallback();
        }
    }
    _load(url,mime){            
        return new Promise((resolve)=>{
            this.get(url).then(text=>{
                let parser = new DOMParser();
                this.document = parser.parseFromString(text, mime);
                resolve(document);
            }).catch(e=>{
                this.exception=e;
            });
        });
    }
    _query(colName,selector,displayFunc){
        if(!this.document)
            return;
        let nodes=[...this.document.querySelectorAll(selector).values()];
        this.data.push({name:colName,data:nodes,func:displayFunc});
        this.headers.add(colName);
    }
    process(){
        for (let h of this.headers)
            this.result.set(h,[]);
        for(let d of this.data){
            let data=d.data.map(d.func);
            this.result.set(d.name,this.result.get(d.name).concat(data));
        }
    }
}