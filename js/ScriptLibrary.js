export default class ScriptLibrary{
    constructor(apiUrl='api/library.php'){
        this.apiUrl=apiUrl;
    }
    create(description='',code=''){
        return new Promise((resolve,reject)=>{
            code = JSON.stringify(code);
            let method='post';
            let headers={'Content-type': 'application/json'};
            let body = JSON.stringify({description, code});
            fetch(this.apiUrl,{method,headers,body}).then(response=>response.text()).then(function(data){
                resolve(data);
            })
        });
    }
    all(){
        return new Promise((resolve,reject)=>{
            fetch(this.apiUrl).then(response=>response.json()).then(function(data){
                resolve(data);
            })
        });
    }
    get(name){
        name=encodeURIComponent(name);
        return new Promise((resolve,reject)=>{
            fetch(`${this.apiUrl}?name=${name}`).then(response=>response.json()).then(function(data) {
                if(!data.code)
                    data.code='';
                resolve(data);
            })
        });
    }
    set(name,code=''){
        name=encodeURIComponent(name);
        return new Promise((resolve,reject)=>{
            let method='post';
            let body = code;
            fetch(`${this.apiUrl}?name=${name}`,{method,body}).then(function(){
                resolve();
            })
        });
    }
    del(name){
        name=encodeURIComponent(name);
        return new Promise((resolve,reject)=>{
            let method='post';
            fetch(`${this.apiUrl}?name=${name}`,{method}).then(function(){
                resolve();
            })
        });
    }
}