<?php
$HOST = "localhost";
$USER = "global";
$PASSWORD = "global";
$DB = "global";
$TABLE = "web_crawler__library";

header("Access-Control-Allow-Origin: *");
$conn = new mysqli($HOST, $USER, $PASSWORD, $DB);

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':
        if(isset($_REQUEST['name']))
            get($_REQUEST['name']);
        else
            all();
        break;
    case 'POST':
        $code=file_get_contents("php://input");
        if((strlen($code)>0) && isset($_REQUEST['name']))
            set($_REQUEST['name'],$code);
        else if(isset($_REQUEST['name']))
            del($_REQUEST['name']);
        break;
    default:
        http_response_code(400); exit();
}

function set($name, $code){
    global $conn,$TABLE;
    $code=$conn->escape_string($code);
    del($name);
    $sql = "insert into {$TABLE}(script_name,code) values('{$name}','{$code}')";
    $conn->query($sql);
}

function del($name){
    global $conn,$TABLE;
    $sql = "delete from {$TABLE} where script_name='{$name}'";
    $conn->query($sql);
}

function get($name){
    global $conn,$TABLE;
    $sql = "select * from {$TABLE} where script_name='{$name}'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo json_encode(['name'=>$row['script_name'], 'code'=>$row['code']]);
}

function all(){
    global $conn,$TABLE;
    $sql = "select script_name from {$TABLE}";
    $result = $conn->query($sql);
    $rows=[];
    while($row = $result->fetch_assoc())
        $rows[]=$row['script_name'];
    echo json_encode($rows);
}

$conn->close();