<?php
header("Access-Control-Allow-Origin: *");

if(isset($_GET['url'])){
    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $_GET['url'],
        CURLOPT_SSL_VERIFYPEER => false
    ]);
    $result=curl_exec($curl);
    if(!$result)
        http_response_code(404);
    else
        echo $result;
    curl_close($curl);
}