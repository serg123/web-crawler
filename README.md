### Description
- use table object in your script to interact with the system
- table.load(url,mime) - loads new document
- table.query(columnName,cssSelector,resultMapCallback) - creates column
- these calls are recorded and played after script is executed
- if there is an error during loading or callback, then call execution chain is stopped

### Examples
```
table.load('https://ilm.ee/kala','text/html');
table.query('date','table.solutab td.kuup',x=>x.innerHTML);
table.query('state','table.solutab td>img',x=>x.src.match(/[a-z_]+(?=\.png)/)[0]);
```

```
for(let n=0;n<=50;n+=25){
  table.load('https://www.cvkeskus.ee/uued-toopakkumised?start='+n,'text/html');
  table.query('job','#f_jobs_main td:nth-child(4) a',x=>x.innerHTML);
  table.query('employer','#f_jobs_main td:nth-child(4) span',x=>x.innerHTML);
  table.query('place','#f_jobs_main td:nth-child(6) span:nth-child(1)',x=>x.innerHTML);
  table.query('logo','#f_jobs_main td:nth-child(3) a',x=>x.innerHTML);
}
```

```
let e = document.createElement('div');
table.load('https://www.cv.ee/toopakkumised/1d/3d','text/html');
table.query('job','.cvo_module_offer',x=>{e.innerHTML=''; e.appendChild(x.cloneNode(true)); return e.innerHTML});
```