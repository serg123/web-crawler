import Controls from './components/Controls.js';
import Result from './components/Result.js';
import ResponseParser from './js/ResponseParser.js';
import ScriptLibrary from './js/ScriptLibrary.js';

window.app = new Vue({
    el:'#app',
    components:{controls:Controls, result:Result},
    template:`<div>
        <controls @save="save" @delete="del" @run="run" @select="load" :names="scriptNames" :name="scriptName" :code="scriptCode"></controls>
        <result :data="result" :loading="loading"></result>
    </div>`,
    data(){
        return{
            result:[],
            scriptNames:[],
            scriptName:'',
            scriptCode:'',
            loading: false
        };
    },
    methods:{
        save(scriptName,scriptCode){
            this.library.set(scriptName,scriptCode).then(data=>{
                this.update(scriptName);
            });
        },
        del(scriptName){
            this.library.del(scriptName).then(data=>{
                this.update();
            });
        },
        run(code){
            this.loading=true;
            let table=new ResponseParser();
            eval(code);
            table.run(()=>{
                this.result=table.result;
                this.loading=false;
            });
        },
        load(scriptName){
            this.scriptCode='';
            this.library.get(scriptName).then(data=>{
                this.scriptName=data.name;
                this.scriptCode=data.code;
            });
        },
        update(scriptName){
            this.library.all().then(data=>{
                this.scriptNames=data;
                if(scriptName)
                    this.load(scriptName);
                else if(data.length>0)
                    this.load(data[0]);
                else{
                    this.scriptName='';
                    this.scriptCode='';
                }
            });
        }
    },
    created(){
        this.library=new ScriptLibrary();
        this.update();
    }
});


function makeTable(table){
    let rowCount=Math.max(...[...table.values()].map(x=>x.length));
    let html='<table><tr>';
    for(let header of table.keys())
        html+=`<th>${header}</th>`;
    html+='</tr>';
    for(let i=0;i<rowCount;i++){
        html+='<tr>';
        for(let header of table.keys()){
            let col=table.get(header);
            if(col.length>i)
                html+=`<td>${col[i]}</td>`;
            else
                html+=`<td></td>`;
        }
        html+='</tr>';
    }
    return html+'</table>';
}