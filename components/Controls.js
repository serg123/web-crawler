export default {
    template: `
<div>
    <nav>
        <div class="combobox">
            <select @change="$emit('select',$event.target.value)" :value="name">
                <option v-for="n in names" :value="n">{{n}}</option>
            </select>
            <input type="text" v-model="name" maxlength="50">
        </div>
        <div style="flex-grow:1">
            <button class="success" style="background:#0b0;" @click="$emit('save',name,codemirror.getValue())">Save</button>
            <button class="danger" style="background:#e00" @click="$emit('delete',name)">Delete</button>
            <button class="primary" style="float:right; background:#09f; font-weight:bold" @click="$emit('run',codemirror.getValue())">&#9658; Run</button>
        </div>
        <textarea id="codemirror"></textarea>
    </nav>
</div>
`,
    props:['names', 'name', 'code'],
    mounted(){
        this.codemirror = CodeMirror.fromTextArea(document.getElementById("codemirror"), {
            mode:  "javascript"
        });
    },
    watch:{
        code(value){
            this.codemirror.getDoc().setValue(value);
        }
    }
}