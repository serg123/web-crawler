export default {
    template: `
<div class="table-container">
    <table v-if="!loading">
        <tr>
            <th v-for="header in headers">{{header}}</th>
        </tr>
        <tr v-for="row in rows">
            <td v-for="cell in row" v-html="cell"></td>
        </tr>
    </table>
        <h3 v-if="loading">Loading ...</h3>
</div>
`,
    props:['data', 'loading'],
    computed:{
        headers(){
            let result=[];
            for(let header of this.data.keys())
                result.push(header);
            return result;
        },
        rows(){
            let rowCount=Math.max(...[...this.data.values()].map(x=>x.length));
            let result=[];
            for(let i=0;i<rowCount;i++){
                result.push([]);
                for(let header of this.data.keys()){
                    let col=this.data.get(header);
                    if(col.length>i)
                        result[i].push(col[i]);
                    else
                        result[i].push('');
                }
            }
            return result;
        }
    }
}